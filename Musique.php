<!DOCTYPE html>
<html>
<head>
<?php
include('config.php'); 
//Récupération de l'id de l'album
$AlbumID = $_GET['AlbumID'];
//Récupération des musique de l'album
$musique = "SELECT * FROM music WHERE fk_album = $AlbumID";
$resultMusique = $mysqli->query($musique);
//Récupération du nom de l'album
$album = "SELECT name FROM album WHERE idAlbum = $AlbumID";
$resultAlbum = $mysqli->query($album);
//Récupération du background de l'album
$background = "SELECT background FROM album WHERE idAlbum = $AlbumID";
$resultBackground = $mysqli->query($background);
$rowBackground = $resultBackground->fetch_assoc();
?>
<title>Juke-Box</title>
<style>

body, html {
  height: 100%;
  margin: 3;
}
.bgImage {
  background-image: url("background/<?php echo $rowBackground["background"]?>");
  filter: blur(10px);
  height: 100%; 
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

.musicBox {
  background-color: rgba(0,0,0, 0.4);
  color: white;
  font-weight: bold;
  font-size: 20px;
  border: 2px solid #f1f1f1;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 400px;
  padding: 20px;
  text-align: center;
  max-height: 70%;
  overflow: auto;
}

.musicTitle {
	display: block;
}

.musicRow {
	display: block;
	margin-bottom: 5px;
	margin-top: 30px;
}

</style>
</head>

	<body>
	<div class="bgImage"></div>
<?php
if ($resultMusique) {
  echo'
  <div class="musicBox">';
  if ($rowAlbum = $resultAlbum->fetch_assoc()) {
  echo '<h1 style="text-align:center; text-decoration: underline">'. $rowAlbum["name"] .'</h1>';
  }
  else {
  echo 'Aucun nom pour album';
	}
  while($rowMusic = $resultMusique->fetch_assoc()) {
    echo'
	<div name="rowPlayer">
	<div id="musicRow" class="musicRow" name="'. $rowMusic["name"].'">
		<div id="'. $rowMusic["idMusic"].'">
				<div class="musicTitle">'. $rowMusic["name"].'</div>
				<audio id="musicPlayer" class="musicPlayer" controls>
				<source src="music/'. $rowMusic["link"].'" type="audio/mpeg"></audio><button onclick="nextUpPlus(this)" title="Ajouter à la liste d\'attente">+</button>
				<br>
			</div>
		</div>
	</div>
	';
  }
  echo'
	</div>
';
} else {
  echo 'Aucun résultat';
}

include('Player.php');
$mysqli->close();
?>
<script>
var elements = document.getElementsByClassName("musicPlayer");
for(var i=0; i<elements.length; i++) {
    elements[i].volume = 0.02;
}
document.getElementById("player").innerHTML = localStorage.getItem("listPlayer");
document.getElementById("currentPlayName").innerHTML = localStorage.getItem("nameKeeper");
document.getElementById("currentPlay").innerHTML = localStorage.getItem("audioKeeper");
</script>
	</body>
</html>