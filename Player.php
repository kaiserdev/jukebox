<div id="nextUpWindow" class="nextUpWindow">
	<div class="nextUpHeader">
		<div class="nextUpTitle">
		Suivant:
		</div>
		<button id="btnCross" class="btnCross" onclick="hideNextUpWindow()" title="Masquer la liste d'attente">X</button>
		<button id="btnDelete" class="btnDelete" onclick="clearAllNextUp()" title="Effacer la liste d'attente">Effacer</button>
	</div>
	<div class="nextUpPlayer">
		<div id="player">
		</div>
	</div>
</div>

<div class="footer">
	<div class="bottomPlayer">
		<a id="buttonReturn" href="Album.php" class="buttonReturn" title="Retourner à l'accueil"><i class="material-icons">home</i></a>
		<div class="musicPlayerBottom">
		<div id="currentPlayName" class="currentPlayName"></div>
		<div id="currentPlay"></div>
		<button id="buttonShow" class="buttonShow" onclick="hideNextUpWindow()" title="Afficher la liste d'attente">Afficher la liste d'attente</button>
		<button id="buttonShowMin" class="buttonShowMin" onclick="hideNextUpWindow()" title="Afficher la liste d'attente">
		<i class="material-icons">menu_open</i>
		</button>
		</div>
	</div>
</div>

<style>
.nextUpWindow {
	position: fixed;
	right: 0;
	bottom: 80px;
	height: 80%;
	width: 400px;
	background-color: #949494;
	border-radius: 5px;
	font-family: sans-serif;
	display: none;
	box-shadow: 0px 0px 5px 3px grey;
	margin: 20px;
	<!-- overflow: auto;-->
	color: #000000;
}

.nextUpTitle {
	padding: 10px 25px;
	font-size: 20px;
	font-family: sans-serif;
	text-decoration: underline;
}

.nextUpHeader {
	display: flex;
	border-bottom: 1px solid;
}

.musicTitle {
	text-align : center;
	font-size: 25px;
	margin-bottom: 3px;
	font-family: sans-serif;
}

.nextUpPlayer {
	text-align: center;
	margin-bottom: 10px;
	max-height: 646px;
	overflow: auto;
}

.btnDelete {
	position: fixed;
	right: 70px;
	margin-top: 10px;
	color: #000000;
	font-size: 14px;
	cursor: pointer;
}

.btnCross {
	position: fixed;
	right: 40px;
	margin-top: 10px;
	color: #000000;
	font-size: 14px;
	cursor: pointer;
}

.footer {
	padding-bottom: 80px;
	color: #000000;
}

.bottomPlayer {
    position: fixed;
    bottom: 0;
	left: 0;
    width: 100%;
	height: 80px;
	background-color: #867b69;
	border-radius: 5px;
	text-align: center;
}

.currentPlayName {
	position: fixed;
	left: 100px;
	bottom: 25px;
	font-size: 20px;
	font-family: sans-serif;
}

.musicPlayerBottom {
	margin-top: 15px;
}

.buttonShow {
	position: fixed;
	right: 10px;
	bottom: 10px;
	color: #000000;
	font-size: 20px;
	cursor: pointer;
}

.buttonReturn {
	position: fixed;
	left: 10px;
	bottom: 10px;
	background-color: white;
    border-radius: 2px;
    color: black;
    cursor: pointer;
}

.buttonShowMin {
	position: fixed;
	right: 10px;
	bottom: 10px;
	display: none;
	cursor: pointer;
}

@media screen and (min-width: 100px) {
.buttonShow {
	display: none;
	}

.buttonShowMin {
	display: block;
	}
	
.buttonReturn {
	position: fixed;
	left: 10px;
	bottom: 10px;
	}
}

@media screen and (min-width: 800px) {
	.buttonShow {
	display: block;
	}

.buttonShowMin {
	display: none;
	}
	
.buttonReturn {
	position: fixed;
	left: 10px;
	bottom: 10px;
	}
}
</style>
<script>
//Ajouter une musique à la liste d'attente
function nextUpPlus(elem) {
  //id = (elem.parentNode.parentNode.id);
  //var itm = document.getElementById(id);
  //var cln = itm.cloneNode(true);
  var name = (elem.parentNode.parentNode.getAttribute("name"));
   
  var divRow = document.createElement("DIV");
  divRow.setAttribute("name", "musicPlayerRow");
  divRow.setAttribute("class", "musicRow");
  
  var divText = document.createElement("DIV");
  divText.setAttribute("name", "divText");
  
  var audio = document.createElement("AUDIO");
  audio.setAttribute("class", "musicPlayer");
  var audioControls = document.createAttribute("controls");
  audio.setAttributeNode(audioControls);
  
  var btn = document.createElement("BUTTON");
  btn.setAttribute("title", "Ajouter à la liste d'attente");
  btn.setAttribute("onclick", "nextUpPlus(this)");
  btn.innerHTML = "+";
  
  var btnCross = document.createElement("BUTTON");
  btnCross.setAttribute("title", "Supprimer de la liste d'attente");
  btnCross.setAttribute("onclick", "clearNextUp(this)");
  btnCross.innerHTML = "X";
  
  //Distinction entre la musique de l'album et celle de la liste d'attente
  if (name != "musicPlayerRow") {
	  var divName = document.createElement("DIV");
	  divName.setAttribute("name", name);
  
	  var divTitle = document.createElement("DIV");
	  divTitle.setAttribute("class", "musicTitle");
	  divTitle.innerHTML = name;
	  
	  var source = (elem.previousSibling.lastChild.src);
	  var src = document.createElement("SOURCE");
	  src.setAttribute("src", source);
	  src.setAttribute("type", "audio/mpeg");
	  
	  document.getElementById("player").appendChild(divRow);
	  divRow.appendChild(divName);
	  divName.appendChild(divTitle);
	  divName.appendChild(divText);
	  divText.appendChild(audio);
	  audio.appendChild(src);
	  divName.appendChild(btn);
	  divName.appendChild(btnCross);
	  
	  //Ajout de la musique ajoutée dans le lecteur du bas, mais interrompt la lecture en cours
	  currentAudio = divName.lastChild.previousSibling.previousSibling.innerHTML;
	  document.getElementById("currentPlay").innerHTML = currentAudio;
	  keepAudio = document.getElementById("currentPlay").innerHTML;
	  localStorage.setItem("audioKeeper", keepAudio);
	  
	  currentName = divTitle.innerHTML;
	  document.getElementById("currentPlayName").innerHTML = currentName;
	  keepName = document.getElementById("currentPlayName").innerHTML;
	  localStorage.setItem("nameKeeper", keepName);
	  
	  //Sauvegarde de la liste de lecture
	  player = document.getElementById("player").innerHTML;
	  localStorage.setItem("listPlayer", player);
	  
  } else {
	  var name = (elem.parentNode.getAttribute("name"));
	  
	  var divName = document.createElement("DIV");
	  divName.setAttribute("name", name);
  
	  var divTitle = document.createElement("DIV");
	  divTitle.setAttribute("class", "musicTitle");
	  divTitle.innerHTML = name;
  
	  var source = (elem.previousSibling.lastChild.lastChild.src);
	  var src = document.createElement("SOURCE");
	  src.setAttribute("src", source);
	  src.setAttribute("type", "audio/mpeg");
  
	  document.getElementById("player").appendChild(divRow);
	  divRow.appendChild(divName);
	  divName.appendChild(divTitle);
	  divName.appendChild(divText);
	  divText.appendChild(audio);
	  audio.appendChild(src);
	  divName.appendChild(btn);
	  divName.appendChild(btnCross);
	  
	  //Ajout de la musique ajoutée dans le lecteur du bas, mais interrompt la lecture en cours
	  currentAudio = divName.lastChild.previousSibling.previousSibling.innerHTML;
	  document.getElementById("currentPlay").innerHTML = currentAudio;
	  keepAudio = document.getElementById("currentPlay").innerHTML;
	  localStorage.setItem("audioKeeper", keepAudio);
	  
	  currentName = divTitle.innerHTML;
	  document.getElementById("currentPlayName").innerHTML = currentName;
	  keepName = document.getElementById("currentPlayName").innerHTML;
	  localStorage.setItem("nameKeeper", keepName);
	  
	  //Sauvegarde de la liste de lecture
	  player = document.getElementById("player").innerHTML;
	  localStorage.setItem("listPlayer", player);
  }
  //document.getElementById("player").appendChild(cln);
  //document.getElementById("player").lastElementChild.appendChild(btn);
  //newid = (document.getElementById("player").appendChild(cln).id);
  //document.getElementById(newid).appendChild(btn);
  
   /*
  id = (elem.parentNode.parentNode.id);
  var itm = document.getElementById(id);
  var cln = itm.cloneNode(true);
  var btn = document.createElement("BUTTON");
  var btnTitle = document.createAttribute("title");
  var btnOnclick = document.createAttribute("ONCLICK");
  document.getElementById("player").appendChild(cln);
  document.getElementById("player").lastElementChild.appendChild(btn);
  */
}

//Afficher et cacher la liste d'attente
function hideNextUpWindow() {
	var nextUpWindow = document.getElementById("nextUpWindow");
	var buttonShow = document.getElementById("buttonShow");
	if (nextUpWindow.style.display === "block") {
    nextUpWindow.style.display = "none";
    buttonShow.innerHTML = "Afficher la liste d'attente";
  } else {
    nextUpWindow.style.display = "block";
    buttonShow.innerHTML = "Cacher la liste d'attente";
  }
}

//Supprimer la musique de la liste d'attente
function clearNextUp(element) {
	element.parentNode.parentNode.parentNode.removeChild(element.parentNode.parentNode);
	player = document.getElementById("player").innerHTML;
	localStorage.setItem("listPlayer", player);
}

//Supprimer toutes les musiques de la liste d'attente
function clearAllNextUp() {
	player = document.getElementById("player").innerHTML = "";
	localStorage.setItem("listPlayer", player);
}

//Jouer un seul audio à la fois
function playOne(player) {
  player.addEventListener("play", function(event) {
  audio_elements = player.getElementsByTagName("audio")
    for(i=0; i < audio_elements.length; i++) {
      audio_element = audio_elements[i];
      if (audio_element !== event.target) {
        audio_element.pause();
      }
    }
  }, true);
}

//Charge la fonction "playOne" dès que l'HTML est chargé
document.addEventListener("DOMContentLoaded", function() {
  playOne(document.body);
});
</script>