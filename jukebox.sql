-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 06 oct. 2020 à 14:44
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `jukebox`
--

-- --------------------------------------------------------

--
-- Structure de la table `album`
--

DROP TABLE IF EXISTS `album`;
CREATE TABLE IF NOT EXISTS `album` (
  `idAlbum` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `background` varchar(255) NOT NULL,
  `fk_category` int(255) NOT NULL,
  PRIMARY KEY (`idAlbum`),
  KEY `id_category` (`fk_category`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `album`
--

INSERT INTO `album` (`idAlbum`, `name`, `description`, `cover`, `background`, `fk_category`) VALUES
(1, 'Mirror\'s Edge', 'Calm - Electro', 'Mirrors Edge', 'Mirrors Edge', 1),
(2, 'The World Ends with You', 'Pop - Rock - Electro - Hip-hop', 'TWEWY', 'TWEWY', 1),
(3, 'Distant Worlds III', 'Orchestral', 'Distant Worlds', 'Final Fantasy', 1),
(4, 'Final Fantasy XV', 'Epique - Orchestral', 'FF XV', 'FF XV', 1),
(5, 'Street Fighter V', 'Rock - Electro - Metal', 'SFV', 'SFV', 1),
(6, 'Metallica', 'Metal', 'Metallica', 'Metallica', 2),
(7, 'Powerwolf', 'Heavy Metal', 'Powerwolf', 'Powerwolf', 2),
(8, 'Gloryhammer', 'Power Metal', 'Gloryhammer', 'Gloryhammer', 2),
(9, 'Alestorm', 'Pirate Metal', 'Alestorm', 'Alestorm', 2),
(10, 'Papa Roach', 'Rock Alternatif', 'Papa Roach', 'Papa Roach', 2),
(11, 'Black Tide', 'Heavy Metal', 'Black Tide', 'Black Tide', 2),
(12, 'George Strait', 'Country', 'George Strait', 'George Strait', 3),
(13, 'Ennio Morricone', 'Country - BO', 'Ennio Morricone', 'Ennio Morricone', 3),
(14, 'John Denver', 'Country', 'John Denver', 'John Denver', 3),
(15, 'Johnny Cash', 'Country - Calme', 'Johnny Cash', 'Johnny Cash', 3),
(16, 'Kenny Rogers', 'Country - Old school', 'Kenny Rogers', 'Kenny Rogers', 3),
(17, 'Alan Jackson', 'Country', 'Alan Jackson', 'Alan Jackson', 3),
(18, 'Shovel Knight', 'Old school - 8-bit', 'Shovel Knight', 'Shovel Knight', 1),
(19, 'Michael Jackson', 'Pop', 'Michael Jackson', 'Michael Jackson', 4),
(20, 'a-ha', 'Pop rock', 'a-ha', 'a-ha', 4),
(21, 'Rick Astley', 'Pop', 'Rick Astley', 'Rick Astley', 4),
(22, 'Survivor', 'Pop rock', 'Survivor', 'Survivor', 4),
(23, 'Queen', 'Pop rock', 'Queen', 'Queen', 4),
(24, 'The Bangles', 'Pop rock', 'The Bangles', 'The Bangles', 4);

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `idCategory` int(255) NOT NULL AUTO_INCREMENT,
  `genre` varchar(255) NOT NULL,
  PRIMARY KEY (`idCategory`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`idCategory`, `genre`) VALUES
(1, 'OST Jeux'),
(2, 'Metal/Rock'),
(3, 'Country'),
(4, 'Pop/New wave');

-- --------------------------------------------------------

--
-- Structure de la table `music`
--

DROP TABLE IF EXISTS `music`;
CREATE TABLE IF NOT EXISTS `music` (
  `idMusic` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `fk_album` int(255) NOT NULL,
  PRIMARY KEY (`idMusic`),
  KEY `id_album` (`fk_album`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `music`
--

INSERT INTO `music` (`idMusic`, `name`, `link`, `fk_album`) VALUES
(1, 'Calling', '02. Calling', 2),
(2, 'SOMEDAY -KINGDOM MIX-', '59. SOMEDAY -KINGDOM MIX-', 2),
(3, 'You\'re Not Alone (FFIX)', '4. You\'re Not Alone (FFIX)', 3),
(4, 'Balamb Garden ~ Ami (FFVIII)', '5. Balamb Garden ~ Ami (FFVIII)', 3),
(5, 'Blinded by Light (FFXIII)', '8. Blinded by Light (FFXIII)', 3),
(6, 'Hangover (Taio Cruz Cover)', 'Alestorm - Hangover (Taio Cruz Cover)', 9),
(7, 'You Are A Pirate', 'Alestorm - You Are A Pirate', 9),
(8, 'Warriors Of Time', 'Black Tide - Warriors Of Time', 11),
(9, 'EPISODE IGNIS - The Main Theme', 'FF XV - EPISODE IGNIS - The Main Theme', 4),
(10, 'Somnus', 'FF XV - Somnus', 4),
(11, 'Up for the Challenge', 'FF XV - Up for the Challenge', 4),
(12, 'Angus McFife', 'Gloryhammer - Angus McFife', 8),
(13, 'Rise Of The Chaos Wizards', 'Gloryhammer - Rise Of The Chaos Wizards', 8),
(14, 'Universe On Fire', 'Gloryhammer - Universe On Fire', 8),
(15, 'Lisa Miskovsky - Still Alive', 'Lisa Miskovsky - Still Alive', 1),
(16, 'Cyanide', 'Metallica - Cyanide', 6),
(26, 'A Fire I Can\'t Put Out', 'George Strait - A Fire I Can\'t Put Out', 12),
(18, 'Burn', 'Papa roach - Burn', 10),
(19, 'Out In The Fields', 'Powerwolf - Out In The Fields', 7),
(20, 'Son of a wolf', 'Powerwolf - Son of a wolf', 7),
(21, 'Main Menu Arcade Edition', 'Main Menu Arcade Edition', 5),
(22, 'Dojo -Japan Stage-', 'Dojo -Japan Stage-', 5),
(23, 'Theme of Guile', 'Theme of Guile', 5),
(24, 'Theme of Zeku', 'Theme of Zeku', 5),
(25, 'Solar Fields - Introduction', 'Solar Fields - Introduction', 1),
(27, 'Right Or Wrong', 'George Strait - Right Or Wrong', 12),
(28, 'For A Few Dollars More', 'Ennio Morricone - For A Few Dollars More', 13),
(29, 'The Ecstacy of Gold', 'Ennio Morricone - The Ecstacy of Gold', 13),
(30, 'The Good, the Bad and the Ugly', 'Ennio Morricone - The Good, the Bad and the Ugly', 13),
(31, 'Annie\'s Song', 'John Denver - Annie\'s Song', 14),
(32, 'Take Me Home, Country Roads', 'John Denver - Take Me Home, Country Roads', 14),
(33, 'Ain\'t No Grave', 'Johnny Cash - Ain\'t No Grave', 15),
(34, 'Hurt', 'Johnny Cash - Hurt', 15),
(35, 'God\'s Gonna Cut You Down', 'Johnny Cash - God\'s Gonna Cut You Down', 15),
(36, 'Rusty Cage', 'Johnny Cash - Rusty Cage', 15),
(37, 'The Man Comes Around', 'Johnny Cash - The Man Comes Around', 15),
(38, 'The Gambler', 'Kenny Rogers - The Gambler', 16),
(39, 'All I Ever Need Is You (with Dottie West)', 'Kenny Rogers and Dottie West - All I Ever Need Is You', 16),
(40, 'Chattahoochee', 'Alan Jackson - Chattahoochee', 17),
(41, 'It\'s Five O\'Clock Somewhere (with Jimmy Buffett)', 'Alan Jackson - It\'s Five O\'Clock Somewhere', 17),
(42, 'Fighting with All of Our Might', 'Shovel Knight - Fighting with All of Our Might', 18),
(43, 'Of Devious Machinations (Clockwork Tower)', 'Shovel Knight - Of Devious Machinations (Clockwork Tower)', 18),
(44, 'Strike the Earth! (Plains of Passage)', 'Shovel Knight - Strike the Earth! (Plains of Passage)', 18),
(45, 'The Defender (Black Knight Battle)', 'Shovel Knight - The Defender (Black Knight Battle)', 18),
(46, 'Thriller', 'Michael Jackson - Thriller', 19),
(47, 'Beat It', 'Michael Jackson - Beat It', 19),
(48, 'Smooth Criminal', 'Michael Jackson - Smooth Criminal', 19),
(49, 'Billie Jean', 'Michael Jackson - Billie Jean', 19),
(50, 'Take on Me', 'a-ha - Take on Me', 20),
(51, 'Summer Moved On', 'a-ha - Summer Moved On', 20),
(52, 'The Sun Always Shines on TV', 'a-ha - The Sun Always Shines on TV', 20),
(53, 'Never Gonna Give You Up', 'Rick Astley - Never Gonna Give You Up', 21),
(54, 'Together Forever', 'Rick Astley - Together Forever', 21),
(55, 'Another One Bites The Dust', 'Queen - Another One Bites The Dust', 22),
(56, 'Dont Stop Me Now', 'Queen - Dont Stop Me Now', 22),
(57, 'We Are The Champions', 'Queen - We Are The Champions', 22),
(58, 'Eye of the Tiger', 'Survivor - Eye of the Tiger', 23),
(59, 'Burning Heart', 'Survivor - Burning Heart', 23),
(60, 'Manic Monday', 'The Bangles - Manic Monday', 24),
(61, 'Walk Like an Egyptian', 'The Bangles - Walk Like an Egyptian', 24),
(62, 'D&eacute;j&agrave; Vu', '39. Deja Vu', 2),
(63, 'Game Over', '04. Game Over', 2),
(64, 'Long Dream', '08. Long Dream', 2),
(65, 'The One Star', '50. The One Star', 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
