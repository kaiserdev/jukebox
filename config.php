<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<?php
$user = 'root';
$password = '';
$database = 'jukebox';
$port = NULL; //NULL pour port par défaut
$mysqli = new mysqli('127.0.0.1', $user, $password, $database, $port); //Connection à la BD

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}
?>