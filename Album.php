<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>♪ Juke-Box ♪</title>
</head>
<?php include('config.php'); ?>
<style>
body
{
    background-color: #f0eed8;
}
</style>
<body>
    <header>
        <h1>♫ Juke-Box ♫</h1>
        <p>Ecoutez des musiques en tout genre.</p>
    </header>
<main>
<?php
//Récupération des albums et leur catégorie
$categories = "SELECT * FROM category";
$resultCategories = $mysqli->query($categories);
$albums = "SELECT * FROM album INNER JOIN category WHERE album.fk_category = category.idCategory";
$resultAlbums = $mysqli->query($albums);
$rowAlbum = $resultAlbums->fetch_assoc();

//Boucle pour afficher les genres et les albums concernés
while ($rowCategory = $resultCategories->fetch_assoc() and $rowAlbum["fk_category"] == $rowCategory["idCategory"]) {
	echo '<h2 id="'.$rowCategory["idCategory"].'">'.$rowCategory["genre"].'</h2>';
	if ($resultAlbums) {
	  echo'
	  <section class="carousel">
		<ul class="carousel-items">';
		do {
			echo'
			<li class="carousel-item">
				<div class="card">
					<h3 class="card-title" title="'.$rowAlbum["name"].'">'.$rowAlbum["name"].'</h3>
					<img src="cover/'.$rowAlbum["cover"].'" />
					<div class="card-content">
						<p class="description" title="'.$rowAlbum["description"].'">'.$rowAlbum["description"].'</p>
						<a id="'.$rowAlbum["idAlbum"].'" href="musique.php?AlbumID='.$rowAlbum["idAlbum"].'" class="button">Ecouter</a>
					</div>
				</div>
			</li>';
		} while($rowAlbum = $resultAlbums->fetch_assoc() and $rowAlbum["fk_category"] == $rowCategory["idCategory"]);
	  echo'
		</ul>
	  </section>
	';
	} else {
	  echo 'Aucun résultat';
	}
}

include('Player.php');
$mysqli->close();
?>
</main>
</body>
<script>
document.getElementById("player").innerHTML = localStorage.getItem("listPlayer");
document.getElementById("currentPlayName").innerHTML = localStorage.getItem("nameKeeper");
document.getElementById("currentPlay").innerHTML = localStorage.getItem("audioKeeper");
</script>
</html>