Pour faire fonctionner l'application :
- Installer et lancer Wampserver
- Déplacer l'application Jukebox dans le dossier "www"
- Ouvrir un navigateur
- Se rendre à l'addresse : "http://localhost/phpmyadmin/index.php" (user : root ; mdp : [vide])
- Créer la Base de données "jukebox"
- Importer les données via le fichier "jukebox.sql"
- Se rendre à l'accueil de l'application : "http://localhost/jukebox/Album.php"